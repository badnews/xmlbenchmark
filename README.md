####Quick benchmark script of 3 XML parsers####

Comparing:

* cElementTree
* ElementTree
* lxml.objectify

Input: the old testament -- 48561L. 3487163C.

The process is to parse the file into the relevant XML object and then walk the tree to find: the first verse of every second chapter within every book.

Run benchmarks:

    $ for i in `ls *py` ; do echo $i >> results && python $i >> results; done
    $ cat results
    bench_cet.py
    total time: 0.262944, single iteration: 0.026294
    bench_et.py
    total time: 3.565562, single iteration: 0.356556
    bench.py
    total time: 0.468966, single iteration: 0.046897

