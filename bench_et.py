# old testament xml parser

import timeit

setup = """
import xml.etree.ElementTree as ET


def xml_filter(xml, path, **kwargs):
    objs = xml.findall(path)
    for k, v in kwargs.iteritems():
        objs = filter(lambda o: getattr(o, k) == v, objs)
    return objs


def xml_filter_2(xml, path, child, **kwargs):
    objs = xml.findall(path)
    for k, v in kwargs.iteritems():
        objs = filter(lambda o: getattr(o.find(child), k) == v, objs)
    return objs


def process():
    xml = ET.parse("ot.xml").getroot()
    books = xml_filter(xml, 'bookcoll/book')
    chapter2s = [xml_filter_2(x, 'chapter', 'chtitle', text="Chapter 2") for x in books]
    chapter2s = [x[0] for x in chapter2s if x]
    verses = ""
    for ch in chapter2s:
        verses += ch.findall('v')[0].text
    return verses
"""

benchmark = """
process()
"""

if __name__ == '__main__':
    time = timeit.timeit(setup=setup, stmt=benchmark, number=50)
    print "total time: %f, single iteration: %f" % (time, time/50.0)
